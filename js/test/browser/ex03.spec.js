const test = require('ava');

function scaffoldStructure() {
  throw new Error('not implemented');
}

test('the creation of a page from scratch', t => {
  const data = [
    { name: 'Bernardo', email: 'b.ern@mail.com' },
    { name: 'Carla', email: 'carl@mail.com' },
    { name: 'Fabíola', email: 'fabi@mail.com' }
  ];

  scaffoldStructure(document, data);

  const nameNodes = document.querySelectorAll('.name');
  t.is(nameNodes.length, data.length);
});
